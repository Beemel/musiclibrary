package MusicLibrary;

import java.io.*;
import java.util.ArrayList;


public class MusicLibraryFileHandler<artistsList> {

    private String filePath;

    public MusicLibraryFileHandler(String filePath) {
        this.filePath = filePath;

    }


    public void readArtistsFromFile (){

    try (FileReader fileReader = new FileReader(filePath);
        BufferedReader bufferedReader = new BufferedReader(fileReader);) {
        String line ="";

        while ((line = bufferedReader.readLine()) !=null) {
            System.out.println(line);
        }
    }
        catch (FileNotFoundException e) {
            System.out.println();
            System.out.println("*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *");
            System.out.println("*                                                                       *");
            System.out.println("*              There is no heavy rocking list file yet =(               *");
            System.out.println("*   But dont worry! Start writing rockers and I'll create one for you   *");
            System.out.println("*                                                                       *");
            System.out.println("*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *");
            System.out.println();
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    public void  writeArtistsToFile (ArrayList artistsList) {
        try (FileWriter fileWriter = new FileWriter(filePath, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {

            for (int i=1 ; i < artistsList.size(); i++) {
              bufferedWriter.write(artistsList.get(i) +"\n");
    }
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }





}
