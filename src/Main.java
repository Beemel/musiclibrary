import MusicLibrary.MusicLibraryFileHandler;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        MusicLibraryFileHandler musicLibraryFileHandler = new MusicLibraryFileHandler("artists.txt");
        musicLibraryFileHandler.readArtistsFromFile();
        System.out.println();

        System.out.println("Write the name of some hard rocking guys and/or gals");
        System.out.println("(type exit to quit and save your list)");
        System.out.println();

        String artist = " ";
        ArrayList<String> artistsList = new ArrayList();

        Scanner inputArtist = new Scanner(System.in);

        do {
            artistsList.add (artist);
            System.out.print("Input Rock! >");
            artist = inputArtist.nextLine();
        }while (!artist.equals("exit"));

        musicLibraryFileHandler.writeArtistsToFile(artistsList);
    }}
